<?php

// Adicionar arquivos .css e .js

function add_styles_and_scripts() {
    
    // all styles
    wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css');
    wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css');

    // all s
    wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.min.js');
    
}
add_action( 'wp_enqueue_scripts', 'add_styles_and_scripts' );

?>
