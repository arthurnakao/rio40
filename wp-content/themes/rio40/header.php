<!DOCTYPE html>
<html lang="en">
<head>
    <title>
        <?php
        if(is_front_page()) { //home
            echo wp_title('');

        } elseif (is_page()) {
            echo wp_title(''); echo ' - ';

        } elseif (is_search()) {
            echo 'Busca - ';

        } elseif (!(is_404()) && (is_single()) || (is_page())) {
            wp_title(''); echo ' - ';

        } elseif (is_404()) {
            echo 'Not Found - ';
            
        } bloginfo('name');
        ?>
    </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <?php wp_head(); ?>
</head>
<body id="landing">

    <nav class="navbar headerPage">
        <div class="headerContainer">
            <a class="navbar-brand itemHeader" href="#">Quem Somos</a>
            <a class="navbar-brand itemHeader" href="#">Portfólio</a>
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/Logo.png" alt="logoRio40">
            <a class="navbar-brand itemHeader" href="#">Contato</a>
            <a class="navbar-brand itemHeader" href="#">Onde Estamos</a>
        </div> 
    </nav>