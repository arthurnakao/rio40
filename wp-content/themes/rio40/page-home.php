<?php 
// Template Name: Home
?>

<?php get_header(); ?>

    <div id="carouselControls" class = "carousel slide" data-ride = "carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselIndicators" data-slide-to="1"></li>
            <li data-target="#carouselIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block w-100 imageSlide" src="<?php the_field('carousel_1') ?>" alt="sorvete">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100 imageSlide" src="<?php the_field('carousel_2') ?>" alt="sorvete">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100 imageSlide" src="<?php the_field('carousel_3') ?>" alt="sorvete">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class = "carousel-control-next" href="#carouselControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon " aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>

    <div class="row about">
        <div class="col-md-5 alignItemsAbout">
            <h2 class="titleAbout">Quem</h2>
            <h2 class="titleAbout">Somos</h2>
            <p> <?php the_field('texto_quem_somos'); ?> </p>
        </div>
        <div class="col-md-5 aboutImage">
            <img src = "<?php the_field('imagem_quem_somos'); ?>" alt="MulherSorvete">
        </div>
    </div>

    <div class="row menu" id="showItemsFirstLine">
        <div class="col-md-6">
            <!-- <img src = "images/Menu1.png" alt="Menu1" class = "col-md-12"> -->
            <div class="leftTopImageDiv">
                <div class="hoverMenu">
                    <h2><?php the_field('titulo_menu_1') ?></h2>
                    <h3><?php the_field('texto_menu_1') ?></h3>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <!-- <img src="images/Menu2.png" alt="picole" class="imageMenu"> -->
            <div class="rightTopImageDiv">
                <div class="hoverMenu">
                    <h2><?php the_field('titulo_menu_2') ?></h2>
                    <h3><?php the_field('texto_menu_1') ?></h3>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row menu" id="showItemsSecondLine">
        <div class="col-md-6">
            <div class="rightBottomImageDiv">
                <div class="hoverMenu">
                    <h2><?php the_field('titulo_menu_3') ?></h2>
                    <h3><?php the_field('texto_menu_1') ?></h3>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="leftBottomImageDiv">
                <div class="hoverMenu">
                    <h2><?php the_field('titulo_menu_4') ?></h2>
                    <h3><?php the_field('texto_menu_4') ?></h3>
                </div>
            </div>
        </div>
    </div>

    <div class = "row contact">
        <div class="col-md-5 alignItemsContact">
            <h2 class="titleContact">Onde</h2>
            <h2 class="titleContact">Estamos</h2>
            <div class="row">
                <h3 class="infoContact">Endereço: </h3>
                <p class="infoDetail"><?php the_field('endereco'); ?></p>
            </div>
            <div class="row">
                <h3 class="infoContact">Telefone: </h3>
                <p class="infoDetail"><?php the_field('telefone'); ?></p>
            </div>
            <div class="row">
                <h3 class="infoContact"> Horário de Funcionamento: </h3>
                <p class="infoDetail"><?php the_field('horario_de_funcionamento'); ?></p>
            </div>
        </div>
        <div class="col-md-5 mapouter">
            <div class="gmap_canvas">
                <iframe width="550" height="400" id="gmap_canvas" src="https://maps.google.com/maps?q=rua%20andrade%20neves%2083&t=&z=17&ie=UTF8&iwloc=&output=embed" 
                frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                <a href="https://www.embedgooglemap.net"></a>
            </div>
            <style>.mapouter{text-align:center;height:100%;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:100%;width:100%;}</style>
        </div>
    </div>  

<?php get_footer(); ?>