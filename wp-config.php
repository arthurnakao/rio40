<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'rio40' );

/** MySQL database username */
define( 'DB_USER', 'admin' );

/** MySQL database password */
define( 'DB_PASSWORD', 'admin' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '{[7b2Ns(*|suJ;-pBPDGUHf7;Z+fS_g]`6Rp;:aL yig_g:. M0?|H3X0<}Ml3wD' );
define( 'SECURE_AUTH_KEY',  '%U(R+b8qQQo4Rj8x_tbHm>^KT1V[i^`QvR]{ff%]Bok-6{T!~qUzS`kG*YD&Tgcj' );
define( 'LOGGED_IN_KEY',    'kV0M6-4-t`x!de^(Dift~p./[%D.a7Ji%q|dZhRXWzZsY-efrU`OrQ^UvIL7iEG ' );
define( 'NONCE_KEY',        'Q-OTc2Sov5>O>B,v%wUp~dqeS,sMCB[~P#Sd5r*SN(EdH)6N,OLn`E]ceqAwgAm9' );
define( 'AUTH_SALT',        '%w>0EFQb-WiaLzyao.8l[;wbh7zkRWB#9ds6ql-d3~IKf4DKD`1dVyT((0x#[)aA' );
define( 'SECURE_AUTH_SALT', 'Tv.0Vv>oPd8R.w!r=Be&W|*YXy9sLnL:nZ{Q?@Myar42( X2X[$rj:WM,&%,Zbqf' );
define( 'LOGGED_IN_SALT',   'L7[cQ:aMlD!zCl8jEH6}w0q-0Is4_B/u>miNxa[DoalpX*j (NUnkvrh{3< .QeV' );
define( 'NONCE_SALT',       ']xv%3JWCU2Q3JB<R(sTK3t0&JF:]b^=CvuLLja>48_W,Ahof=.!B?|>r*}MW/8v2' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );

